# JDK 8

# Fazer download do Java JDK
- https://www.oracle.com/java/technologies/javase-jdk8-downloads.html

# Caso a pasta não exista 
- sudo mkdir /usr/lib/jvm

# Entre na pasta
- cd /usr/lib/jvm

# Descompacte o arquivo
- sudo tar -xvzf ~/Downloads/jdk-8u251-linux-x64.tar.gz

# Editar as variáveis de ambiente
- sudo vi /etc/environment

- PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/jdk1.8.0_251/bin:/usr/lib/jvm/jdk1.8.0_251/db/bin:/usr/lib/jvm/jdk1.8.0_251/jre/bin"
- J2SDKDIR="/usr/lib/jvm/jdk-8u251-linux-x64"
- J2REDIR="/usr/lib/jvm/jdk-8u251-linux-x64/jre"
- JAVA_HOME="/usr/lib/jvm/jdk-8u251-linux-x64"
- DERBY_HOME="/usr/lib/jvm/jdk-8u251-linux-x64/db"

# Entre com os comandos
- sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_251/bin/java" 0
- sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_251/bin/javac" 0
- sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_251/bin/java
- sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_251/bin/javac

# Para validar os comandos
- update-alternatives --list java
- update-alternatives --list javac

# Verificar a versão do Java
- java -version
